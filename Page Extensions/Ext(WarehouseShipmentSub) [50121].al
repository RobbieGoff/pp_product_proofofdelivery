pageextension 50121 "ExtWarehouseShipmentSub" extends "Whse. Shipment Subform"
{
    layout
    {
        addafter("Qty. Outstanding")
        {
            field("Qty. Packed"; "Qty. Packed")
            {
                ApplicationArea = All;
            }
            field(Packed; Packed)
            {
                ApplicationArea = All;
            }
        }
    }
}
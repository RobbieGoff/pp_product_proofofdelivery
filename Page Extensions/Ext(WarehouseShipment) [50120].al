pageextension 50120 "ExtWarehouseShipment" extends "Warehouse Shipment"
{
    layout
    {
        addafter(Shipping)
        {
            group("Proof of Delivery")
            {
                field(DeliveryStatus; DeliveryStatus)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Status';
                }
                field(DeliveryOrderID; DeliveryOrderID)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Order ID';
                }
                field(DeliveryAddressName; DeliveryAddressName)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Address Name';
                }
                field(DeliveryAddress1; DeliveryAddress1)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Address 1';
                }
                field(DeliveryPostcode; DeliveryPostcode)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Postcode';
                }
                field(DeliveryAddressPhoneNumber; DeliveryAddressPhoneNumber)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Address Phone Number';
                }
                field(DeliveryCompanyName; DeliveryCompanyName)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Company Name';
                }
                field(DeliveryVehicle; DeliveryVehicle)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Vehicle';
                }
                field(DeliveryOrderDate; DeliveryOrderDate)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Order Date';
                }
                field(DeliveryNotes; DeliveryNotes)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Notes';
                }
                part(photoBlob; "nH Warehouse Shipment CardPart")
                {
                    ApplicationArea = All;
                    Caption = 'Photo';
                    SubPageLink = "No." = field("No.");
                }
            }
        }
        addafter("Document Status")
        {
            field("Not Packed"; "Not Packed")
            {
                ApplicationArea = All;
            }
        }
    }
}
tableextension 50121 "Warehouse Shipment Line Ext." extends "Warehouse Shipment Line"
{
    fields
    {
        field(50100; Packed; Boolean)
        {
            DataClassification = SystemMetadata;
        }
        field(50101; "Qty. Packed"; Decimal)
        {
            DataClassification = SystemMetadata;
            DecimalPlaces = 0 : 2;
        }
    }
}
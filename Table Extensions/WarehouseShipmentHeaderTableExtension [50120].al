tableextension 50120 "Warehouse Shipment Header Ext." extends "Warehouse Shipment Header"
{
    fields
    {
        field(50100; DeliveryStatus; Text[50])
        {
            DataClassification = SystemMetadata;
        }
        field(50101; Signature; Blob)
        {
            DataClassification = SystemMetadata;
            Subtype = Bitmap;
        }
        field(50102; Photo; Blob)
        {
            DataClassification = SystemMetadata;
            Subtype = Bitmap;
        }
        field(50103; DeliveryVehicle; Code[20])
        {
            DataClassification = SystemMetadata;
            TableRelation = "nH Vehicle";
            Caption = 'Vehicle';
        }
        field(50104; DeliveryCompanyName; Text[50])
        {
            DataClassification = SystemMetadata;
        }
        field(50105; DeliveryPostcode; Code[10])
        {
            DataClassification = SystemMetadata;
        }
        field(50106; DeliveryAddress1; Text[150])
        {
            DataClassification = SystemMetadata;
        }
        field(50107; DeliveryAddressPhoneNumber; Text[50])
        {
            DataClassification = SystemMetadata;
        }
        field(50108; DeliveryOrderID; Text[30])
        {
            DataClassification = SystemMetadata;
        }
        field(50109; DeliveryOrderDate; Date)
        {
            DataClassification = SystemMetadata;
        }
        field(50110; DeliveryNotes; Text[200])
        {
            DataClassification = SystemMetadata;
        }
        field(50111; DeliveryCompleteProofURL; Text[200])
        {
            DataClassification = SystemMetadata;
        }
        field(50112; DeliveryCompleteMetadata; Text[300])
        {
            DataClassification = SystemMetadata;
        }
        field(50113; DeliveryAddressName; Text[100])
        {
            DataClassification = SystemMetadata;
        }
        field(50114; "Not Packed"; Boolean)
        {
            FieldClass = FlowField;
            CalcFormula = - exist ("Warehouse Shipment Line" where("No." = field("No."), Packed = const(false)));
            Caption = 'Fully Packed';
        }
    }
}
page 50121 "nH Warehouse Shipment Lines"
{
    PageType = List;
    Editable = True;
    // ApplicationArea = All;
    // UsageCategory = Lists;
    SourceTable = "Warehouse Shipment Line";

    layout
    {
        area(Content)
        {
            repeater(Lines)
            {
                field("Source Document"; "Source Document")
                {
                    ApplicationArea = All;
                }
                field("Source No."; "Source No.")
                {
                    ApplicationArea = All;
                }
                field("Item No."; "Item No.")
                {
                    ApplicationArea = All;
                }
                field(Description; Description)
                {
                    ApplicationArea = All;
                }
                field(Quantity; Quantity)
                {
                    ApplicationArea = All;
                }
                field("Qty. to Ship"; "Qty. to Ship")
                {
                    ApplicationArea = All;
                }
                field("Qty. Shipped"; "Qty. Shipped")
                {
                    ApplicationArea = All;
                }
                field("Qty. Outstanding"; "Qty. Outstanding")
                {
                    ApplicationArea = All;
                }
                field("Due Date"; "Due Date")
                {
                    ApplicationArea = All;
                }
                field("Unit of Measure Code"; "Unit of Measure Code")
                {
                    ApplicationArea = All;
                }
                field(Packed; Packed)
                {
                    ApplicationArea = All;
                }
                field("Qty. Packed"; "Qty. Packed")
                {
                    ApplicationArea = All;
                }
                field("Qty. Mispicked"; "Qty. Mispicked")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}
page 50120 "nH Vehicles"
{
    PageType = List;
    Editable = True;
    ApplicationArea = All;
    UsageCategory = Lists;
    SourceTable = "nH Vehicle";

    layout
    {
        area(Content)
        {
            repeater("Car Page")
            {
                field("No."; "No.")
                {
                    ApplicationArea = All;
                }
                field(Company; Company)
                {
                    ApplicationArea = All;
                }
                field(Description; Description)
                {
                    ApplicationArea = All;
                }
                field("Registration No."; "Registration No.")
                {
                    ApplicationArea = All;
                }
            }
        }
    }
}
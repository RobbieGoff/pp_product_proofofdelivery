page 50123 "nH Warehouse Shipment CardPart"
{
    PageType = CardPart;
    // ApplicationArea = All;
    // UsageCategory = Administration;
    SourceTable = "Warehouse Shipment Header";

    layout
    {
        area(Content)
        {
            field(Photo; Photo)
            {
                ApplicationArea = All;
            }
            field(Signature; Signature)
            {
                ApplicationArea = All;
            }
        }
    }
}
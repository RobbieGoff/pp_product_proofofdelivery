page 50122 "nH Warehouse Shipment"
{
    PageType = Card;
    Editable = True;
    // ApplicationArea = All;
    // UsageCategory = Administration;
    SourceTable = "Warehouse Shipment Header";

    layout
    {
        area(Content)
        {
            group(General)
            {
                field("No."; "No.")
                {
                    ApplicationArea = All;
                    Editable = False;
                }
                field(DeliveryVehicle; DeliveryVehicle)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Vehicle';
                }
                field(DeliveryStatus; DeliveryStatus)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Status';
                }
                field(DeliveryOrderID; DeliveryOrderID)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Order ID';
                }
                field(DeliveryAddressName; DeliveryAddressName)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Address Name';
                }
                field(DeliveryAddress1; DeliveryAddress1)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Address 1';
                }
                field(DeliveryPostcode; DeliveryPostcode)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Postcode';
                }
                field(DeliveryAddressPhoneNumber; DeliveryAddressPhoneNumber)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Address Phone Number';
                }
                field(DeliveryNotes; DeliveryNotes)
                {
                    ApplicationArea = All;
                    Caption = 'Delivery Notes';
                }
                field(Signature; mySignatureBlobAsBase64)
                {
                    ApplicationArea = All;
                    Visible = false;
                    trigger OnValidate()
                    begin
                        SetSignatureBlobFromBase64(mySignatureBlobAsBase64);
                    end;
                }
                field(Photo; myPhotoBlobAsBase64)
                {
                    ApplicationArea = All;
                    trigger OnValidate()
                    begin
                        SetPhotoBlobFromBase64(myPhotoBlobAsBase64);
                    end;
                }
                field("Location Code"; "Location Code")
                {
                    ApplicationArea = All;
                }
                field("Document Status"; "Document Status")
                {
                    ApplicationArea = All;
                }
            }
        }
    }

    var

        myPhotoBlobAsBase64: Text;
        mySignatureBlobAsBase64: Text;

    trigger OnAfterGetRecord()
    begin
        myPhotoBlobAsBase64 := GetPhotoBlobAsBase64();
        mySignatureBlobAsBase64 := GetSignatureBlobAsBase64();
    end;

    local procedure GetPhotoBlobAsBase64(): Text
    var
        Base64Convert: Codeunit "Base64 Convert";
        InStr: InStream;
    begin
        CalcFields(Photo);
        if not Photo.HasValue then begin
            exit('');
        end;
        Photo.CreateInStream(InStr);
        exit(Base64Convert.ToBase64(InStr));
    end;

    local procedure SetPhotoBlobFromBase64(Base64Text: Text)
    var
        Base64Convert: Codeunit "Base64 Convert";
        OutStr: OutStream;
    begin
        CalcFields(Signature);
        Photo.CreateOutStream(OutStr);
        Base64Convert.FromBase64(Base64Text, OutStr);
        Modify(true);
    end;

    local procedure GetSignatureBlobAsBase64(): Text
    var
        Base64Convert: Codeunit "Base64 Convert";
        InStr: InStream;
    begin
        CalcFields(Photo);
        if not Signature.HasValue then begin
            exit('');
        end;
        Signature.CreateInStream(InStr);
        exit(Base64Convert.ToBase64(InStr));
    end;

    local procedure SetSignatureBlobFromBase64(Base64Text: Text)
    var
        Base64Convert: Codeunit "Base64 Convert";
        OutStr: OutStream;
    begin
        CalcFields(Signature);
        Signature.CreateOutStream(OutStr);
        Base64Convert.FromBase64(Base64Text, OutStr);
        Modify(true);
    end;

}
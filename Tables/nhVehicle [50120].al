table 50120 "nH Vehicle"
{
    DataClassification = SystemMetadata;

    fields
    {
        field(1; "No."; Code[20])
        {
            DataClassification = SystemMetadata;
        }
        field(2; Company; Text[30])
        {
            DataClassification = SystemMetadata;
        }
        field(3; Description; Text[50])
        {
            DataClassification = SystemMetadata;
        }
        field(4; "Registration No."; Code[20])
        {
            DataClassification = SystemMetadata;
        }
    }

    keys
    {
        key(PK; "No.")
        {
            Clustered = true;
        }
    }
}